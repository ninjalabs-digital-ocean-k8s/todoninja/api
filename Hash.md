# Secret old

$2a$13$ClwZkJQLmVLK5cW.ohT2KeInTSLaENzdDRsIcy4v5m9rxQ0YY1Knm

# Secret old adjusted

$bcrypt$v=98$r=13$ClwZkJQLmVLK5cW.ohT2KeInTSLaENzdDRsIcy4v5m9rxQ0YY1Knm

# New

$bcrypt$v=98$r=10$KubOdqtfq3We4B8mbSvGzg$UhuBkPVUCARFdhskLFn7hJazPjoHlgw
