import Env from "@ioc:Adonis/Core/Env";

const databaseConfig: { vapid: { privateKey: string; publicKey: string } } = {
  vapid: {
    privateKey: Env.get("WEBPUSH_VAPID_PRIVATE"),
    publicKey: Env.get("WEBPUSH_VAPID_PUBLIC"),
  },
};

export default databaseConfig;
