import { args, BaseCommand } from "@adonisjs/core/build/standalone";

export default class SendReminder extends BaseCommand {
  /**
   * Command Name is used to run the command
   */
  public static commandName = "send:reminder";

  /**
   * Command Name is displayed in the "help" output
   */
  public static description = "Send a reminder for a task";
  public static settings = {
    loadApp: true,
  };

  @args.string({ description: "The id of the task" })
  public taskId: string;

  public async run() {
    // const Task = await this.application.container.use("App/Models/Task");
    const Task = (await import("App/Models/Task")).default;
    const TaskReminder = (await import("App/Notifications/TaskReminder")).default;
    const task = await Task.find(this.taskId);
    await task?.preload("user");

    if (!task) {
      this.logger.fatal("Task could not be found!");
      return;
    }
    const reminder = new TaskReminder(task);
    console.log(await reminder.toWebPush());
    await reminder.send(task.user);
    this.logger.info("Sent!");
  }
}
