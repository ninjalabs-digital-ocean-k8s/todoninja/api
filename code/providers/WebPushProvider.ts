import { ApplicationContract } from "@ioc:Adonis/Core/Application";

export default class NotificationProvider {
  public static needsApplication = true;
  constructor(protected application: ApplicationContract) {}

  public register() {
    this.application.container.singleton("Adonis/Notification/Channels/WebPush", () => {
      return new (require("../app/Notifications/WebPush").WebPush)();
    });
  }
}
