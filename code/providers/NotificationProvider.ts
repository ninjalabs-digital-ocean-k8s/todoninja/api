import { ApplicationContract } from "@ioc:Adonis/Core/Application";

export default class NotificationProvider {
  public static needsApplication = true;
  constructor(protected application: ApplicationContract) {}

  public register() {
    this.application.container.bind("Adonis/Notification", () => {
      return { Notification: require("../app/Notifications/Notification").Notification };
    });
  }
}
