import { IocContract } from "@adonisjs/fold";
import { DateTime } from "luxon";
import cron from "node-cron";

export default class AppProvider {
  constructor(protected $container: IocContract) {}

  public register() {
    // Register your own bindings
  }

  public boot() {
    // IoC container is ready
  }

  public shutdown() {
    // Cleanup, since app is going down
  }

  public async ready() {
    const SendTaskReminders = (await import("App/Actions/SendTaskReminders")).default;

    cron.schedule("* * * * *", async () => {
      await new SendTaskReminders(DateTime.now()).handle();
    });
  }
}
