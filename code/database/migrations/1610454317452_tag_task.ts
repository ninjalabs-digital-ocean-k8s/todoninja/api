import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateTagTaskTables extends BaseSchema {
  protected tableName = 'tag_task'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('tag_id').unsigned().references('id').inTable('tags').onDelete('CASCADE')
      table.integer('task_id').unsigned().references('id').inTable('tasks').onDelete('CASCADE')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
