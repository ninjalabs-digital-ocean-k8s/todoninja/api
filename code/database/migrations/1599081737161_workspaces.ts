import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Workspaces extends BaseSchema {
  protected tableName = 'workspaces'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.string('name')
      table.string('color')
      table.string('icon')
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
