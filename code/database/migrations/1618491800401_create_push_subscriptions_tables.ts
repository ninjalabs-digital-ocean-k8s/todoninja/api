import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CreatePushSubscriptionsTables extends BaseSchema {
  protected tableName = "push_subscriptions";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.text("payload");
      table.integer("user_id");
      table.timestamps(true);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
