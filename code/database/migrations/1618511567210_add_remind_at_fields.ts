import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Tasks extends BaseSchema {
  protected tableName = "tasks";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.timestamp("remind_at");
      table.timestamp("reminded_at");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("remind_at");
      table.dropColumn("reminded_at");
    });
  }
}
