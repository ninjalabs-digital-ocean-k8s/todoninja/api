import BaseSeeder from "@ioc:Adonis/Lucid/Seeder";
import User from "App/Models/User";

export default class DevSeeder extends BaseSeeder {
  public static developmentOnly = true;

  public async run() {
    const user = await User.create({
      name: "Sebastian Dittrich",
      email: "sebastian.dittrich@live.de",
      password: "password",
    });

    await user.related("workspaces").createMany([{ name: "Home" }, { name: "Work" }]);
  }
}
