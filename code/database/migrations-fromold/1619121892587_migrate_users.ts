import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Users extends BaseSchema {
  protected tableName = "users";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.renameColumn("createdAt", "created_at");
      table.renameColumn("updatedAt", "updated_at");
    });
  }
}
