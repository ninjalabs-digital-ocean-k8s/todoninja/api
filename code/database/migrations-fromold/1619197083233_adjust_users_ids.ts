import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Users extends BaseSchema {
  public async up() {
    this.schema.raw("ALTER TABLE `users` CHANGE `id` `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;");
  }
}
