import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Tasks extends BaseSchema {
  protected tableName = "tasks";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.dateTime("postponed_until");
      table.renameColumn("deadline", "deadline_at");
      table.renameColumn("today", "today_at");
      table.renameColumn("userId", "user_id");
      table.renameColumn("workspaceId", "workspace_id");
      table.renameColumn("doneAt", "done_at");
      table.renameColumn("remindedAt", "reminded_at");
      table.renameColumn("remindAt", "remind_at");
      table.renameColumn("createdAt", "created_at");
      table.renameColumn("updatedAt", "updated_at");
    });

    this.defer(async (db) => {
      const table = db.from(this.tableName);

      for (const { deadline_at, state, id, remind_at } of await table.select("*")) {
        // set deadline_at to remove invalid dates
        const update = (v: object) =>
          db
            .from(this.tableName)
            .where("id", id)
            .update({ deadline_at, remind_at, ...v });
        if (state === 0) {
          // 0 -> Do / Deadline can be set
          await update({ waiting_for: null });
        } else if (state === 1) {
          // 1 -> Postponed / Deadline is postponed_until
          await update({ waiting_for: null, postponed_until: deadline_at, deadline_at: null });
        } else if (state === 2) {
          // 2 -> Calendar
          await update({ postponed_until: deadline_at, waiting_for: null });
        } else if (state === 3) {
          // 3 -> Waiting
          await update({ deadline_at: null, postponed_until: deadline_at });
        }
      }
    });
    this.schema.table(this.tableName, (table) => {
      table.dateTime("done_at").alter();
      table.text("description").alter();

      table.dropColumn("state");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {});
  }
}
