import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class PushSubscriptions extends BaseSchema {
  protected tableName = "push_subscriptions";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.renameColumn("userId", "user_id");
      table.renameColumn("createdAt", "created_at");
      table.renameColumn("updatedAt", "updated_at");
      table.text("payload");
    });
    this.defer(async (db) => {
      const subscriptions = await db.from(this.tableName).select("*");
      for (const { id, endpoint, public_key, auth_token } of subscriptions) {
        await db
          .from(this.tableName)
          .where("id", id)
          .update({
            payload: JSON.stringify({ endpoint, keys: { p256dh: public_key, auth: auth_token } }),
          });
      }
    });
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("endpoint");
      table.dropColumn("public_key");
      table.dropColumn("auth_token");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {});
  }
}
