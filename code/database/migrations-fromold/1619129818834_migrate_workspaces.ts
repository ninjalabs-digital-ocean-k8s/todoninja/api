import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Workspaces extends BaseSchema {
  protected tableName = "workspaces";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.renameColumn("userId", "user_id");
      table.renameColumn("createdAt", "created_at");
      table.renameColumn("updatedAt", "updated_at");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {});
  }
}
