declare module "@ioc:Adonis/Notification" {
  import User from "App/Models/User";

  interface Channels {}

  interface Channel {
    send(user: User, notification: NotificationInterface): unknown;
  }

  interface NotificationInterface {
    via(notifiable: User): readonly (keyof Channels)[];
    send(user: User);
  }

  export const Notification: new () => NotificationInterface;
}
