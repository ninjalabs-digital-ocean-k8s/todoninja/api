declare module "@ioc:Adonis/Notification/WebPush" {
  import User from "App/Models/User";
  import { NotificationInterface } from "@ioc:Adonis/Notification";

  interface WebPushable extends NotificationInterface {
    toWebPush(user: User): unknown;
  }
}

declare module "@ioc:Adonis/Notification" {
  interface Channels {
    WebPush: boolean;
  }
}
