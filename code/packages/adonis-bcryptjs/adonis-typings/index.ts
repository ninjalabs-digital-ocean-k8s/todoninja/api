declare module "@ioc:Adonis/Core/Hash" {
  export interface BcryptJsContract extends HashDriverContract {
    ids: ["bcryptjs"];
  }
}
