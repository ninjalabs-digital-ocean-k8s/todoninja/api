import { ApplicationContract } from "@ioc:Adonis/Core/Application";

export default class BcryptjsProvider {
  public static needsApplication = true;
  constructor(protected application: ApplicationContract) {}

  public register() {
    // Register your own bindings
  }

  public async boot() {
    this.application.container.use("Adonis/Core/Hash").extend("bcryptjs", () => {
      return new (require("../src/Driver").default)();
    });
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
