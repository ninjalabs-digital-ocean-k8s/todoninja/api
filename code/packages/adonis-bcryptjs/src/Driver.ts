import { BcryptJsContract } from "@ioc:Adonis/Core/Hash";
import bcryptjs from "bcryptjs";

export default class BcryptJs implements BcryptJsContract {
  ids: BcryptJsContract["ids"] = ["bcryptjs"];
  constructor() {}

  make(): never {
    throw new Error("Bcryptjs Driver is verify-only!");
  }

  hash(): never {
    throw new Error("Bcryptjs Driver is verify-only!");
  }

  verify(hashedValue: string, plainValue: string) {
    return new Promise<boolean>((resolve, reject) => {
      bcryptjs.compare(plainValue, hashedValue, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      });
    });
  }
  needsReHash() {
    return true;
  }
}
