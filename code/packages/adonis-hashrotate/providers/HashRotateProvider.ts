import { ApplicationContract } from "@ioc:Adonis/Core/Application";

export default class HashRotateProvider {
  public static needsApplication = true;
  constructor(protected application: ApplicationContract) {}

  public register() {
    // Register your own bindings
  }

  public async boot() {
    this.application.container.use("Adonis/Core/Hash").extend("hashrotate", (_, __, config) => {
      return new (require("../src/Driver").default)(config);
    });
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
