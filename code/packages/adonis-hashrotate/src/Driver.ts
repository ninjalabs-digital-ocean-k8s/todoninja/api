import Hash, { HashRotateConfig, HashRotateContract } from "@ioc:Adonis/Core/Hash";

export default class HashRotate implements HashRotateContract {
  ids: HashRotateContract["ids"] = ["hashrotate"];

  constructor(protected config: HashRotateConfig) {}

  primary() {
    return Hash.use(this.config.list[0]);
  }

  make(value: string) {
    return this.primary().make(value);
  }

  hash(value: string) {
    return this.primary().hash(value);
  }

  async verify(hashedValue: string, plainValue: string) {
    for (const driver of this.config.list) {
      try {
        return await Hash.use(driver).verify(hashedValue, plainValue);
      } catch (e) {
        // Try the next driver
      }
    }
    throw new Error("No Driver in the list can verify the hash.");
  }
  needsReHash(hash: string) {
    for (const driver of this.config.list) {
      try {
        return Hash.use(driver).needsReHash(hash);
      } catch (e) {
        // Try the next driver
      }
    }
    throw new Error("No Driver in the list can tell if a rehash is needed.");
  }
}
