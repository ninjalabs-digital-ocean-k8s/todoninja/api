declare module "@ioc:Adonis/Core/Hash" {
  export interface HashRotateContract extends HashDriverContract {
    ids: ["hashrotate"];
  }
  export type HashRotateConfig = {
    driver: "hashrotate";
    list: (keyof HashersList)[];
  };
}
