import { NormalizedQuery } from "@opaquejs/query";
import { GraphQLScalarType, Kind } from "graphql";
import { InputType } from "type-graphql";

export function hasOwnProperty<X extends {}, Y extends PropertyKey>(
  obj: X,
  prop: Y
): obj is X & Record<Y, unknown> {
  return obj.hasOwnProperty(prop);
}

export const parseJsonOpaqueQuery = (
  value: object,
  withGlobal: boolean = true
): NormalizedQuery => {
  const error = new Error(`${value} is not a valid opaque query json representation`);
  if (withGlobal) {
    const result: NormalizedQuery = {};
    if (hasOwnProperty(value, "_skip") && typeof value["_skip"] == "number") {
      result["_skip"] = value["_skip"];
    }
    if (hasOwnProperty(value, "_limit") && typeof value["_limit"] == "number") {
      result["_limit"] = value["_limit"];
    }
    if (hasOwnProperty(value, "_orderBy") && Array.isArray(value["_orderBy"])) {
      result._orderBy = [];
      for (const { key, direction } of value["_orderBy"]) {
        result._orderBy.push({ key, direction });
      }
    }
    return { ...parseJsonOpaqueQuery(value, false), ...result };
  }
  if (
    hasOwnProperty(value, "key") &&
    hasOwnProperty(value, "comparator") &&
    hasOwnProperty(value, "value")
  ) {
    if (typeof value["key"] !== "string" || typeof value["comparator"] !== "string") {
      throw error;
    }
    return { key: value.key, value: value.value as any, comparator: value.comparator as any };
  }
  if (hasOwnProperty(value, "_and") && Array.isArray(value._and)) {
    return { _and: value._and.map((item) => parseJsonOpaqueQuery(item, false)) };
  }
  if (hasOwnProperty(value, "_or") && Array.isArray(value._or)) {
    return { _or: value._or.map((item) => parseJsonOpaqueQuery(item, false)) };
  }
  if (hasOwnProperty(value, "_not") && typeof value._not == "object" && value._not != null) {
    return { _not: parseJsonOpaqueQuery(value._not, false) };
  }
  return {};
};

export const NormalizedQueryType = class {};
export type NormalizedQueryType = NormalizedQuery;

export const NormalizedQueryScalar = new GraphQLScalarType({
  name: "JsonOpaqueQuery",
  description: "A Json string representing an opaue query",
  serialize(value: unknown): string {
    if (typeof value != "object") {
      throw new Error(`Cannot parse ${value} as a JsonOpaqueQuery`);
    }
    return JSON.stringify(value);
  },
  parseValue(value: unknown): NormalizedQuery {
    // check the type of received value
    if (typeof value == "string") {
      return parseJsonOpaqueQuery(JSON.parse(value));
    }
    if (typeof value == "object" && value != null) {
      return parseJsonOpaqueQuery(value);
    }
    throw new Error("JsonOpaqueQuery can only parse string and object values");
  },
  parseLiteral(ast): NormalizedQuery {
    // check the type of received value
    if (ast.kind !== Kind.STRING) {
      throw new Error("JsonOpaqueQuery can only parse string values");
    }
    return parseJsonOpaqueQuery(JSON.parse(ast.value));
  },
});
