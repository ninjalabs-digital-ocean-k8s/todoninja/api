import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import ApolloServer from "@ioc:Apollo/Server";
import PubSub from "@ioc:Apollo/PubSub";
import AuthResolver from "App/Controllers/GraphQl/AuthResolver";
import TaskResolver from "App/Controllers/GraphQl/TaskResolver";
import UsersResolver from "App/Controllers/GraphQl/UsersResolver";
import WorkspaceResolver from "App/Controllers/GraphQl/WorkspaceResolver";
import PushSubscriptionResolver from "App/Controllers/GraphQl/PushSubscriptionResolver";
import { buildSchemaSync } from "type-graphql";
import { DateTime } from "luxon";
import { GraphQLScalarType, Kind } from "graphql";
import { NormalizedQueryType, NormalizedQueryScalar } from "Packages/opaque-type-graphql";

export default new ApolloServer({
  schema: buildSchemaSync({
    resolvers: [
      AuthResolver,
      UsersResolver,
      WorkspaceResolver,
      TaskResolver,
      PushSubscriptionResolver,
    ],
    scalarsMap: [
      { type: NormalizedQueryType, scalar: NormalizedQueryScalar },
      {
        type: DateTime,
        scalar: new GraphQLScalarType({
          name: "LuxonDateTime",
          description: "Luxon DateTime scalar type",
          serialize(value: unknown) {
            if (!(value instanceof DateTime)) {
              throw new Error("DateTimeScalar can only serialize DateTime values");
            }
            return value.toISO();
          },
          parseValue(value: unknown): DateTime {
            if (typeof value !== "string") {
              throw new Error("DateTimeScalar can only parse string values");
            }
            return DateTime.fromISO(value);
          },
          parseLiteral(ast): DateTime {
            if (ast.kind !== Kind.STRING) {
              throw new Error("DateTimeScalar can only parse string values");
            }
            return DateTime.fromISO(ast.value);
          },
        }),
      },
    ],
    pubSub: PubSub,
    authChecker: ({ context }) => {
      return (context as HttpContextContract).auth.check();
    },
  }),
});
