import { Notification } from "@ioc:Adonis/Notification";
import { WebPushable } from "@ioc:Adonis/Notification/WebPush";
import Task from "App/Models/Task";

export default class TaskReminder extends Notification implements WebPushable {
  constructor(public task: Task) {
    super();
  }

  public via() {
    return ["WebPush"] as const;
  }

  public async toWebPush() {
    await this.task.preload("workspace");
    return {
      title: this.task.title,
      body: "Reminder",
      badge: "/img/icons/badge-96x96-black.png",
      icon: "/img/icons/blank.png",
      data: {
        link: this.task.workspace
          ? `/#/workspaces/${this.task.workspace.id}/tasks?task=${this.task.id}`
          : `/#/inbox?task=${this.task.id}`,
      },
    };
  }
}
