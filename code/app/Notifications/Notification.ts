import { Channel } from "@ioc:Adonis/Notification";
import User from "App/Models/User";

export class Notification {
  async send(user: User) {
    const channels: Channel[] = await Promise.all(
      this.via().map((name) =>
        global[Symbol.for("ioc.use")](`Adonis/Notification/Channels/${name}`)
      )
    );

    await Promise.all(channels.map((channel) => channel.send(user, this)));
  }

  via(): any[] {
    return [];
  }
}
