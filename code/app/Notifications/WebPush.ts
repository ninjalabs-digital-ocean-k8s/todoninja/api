import { Channel } from "@ioc:Adonis/Notification";
import webpush from "web-push";
import Config from "@ioc:Adonis/Core/Config";
import User from "App/Models/User";
import { WebPushable } from "@ioc:Adonis/Notification/WebPush";

export class WebPush implements Channel {
  public client = webpush;

  constructor() {
    this.client.setVapidDetails(
      "mailto:info@todoninja.de",
      Config.get("webpush.vapid.publicKey"),
      Config.get("webpush.vapid.privateKey")
    );
  }

  async send(user: User, notification: WebPushable) {
    for (const pushSubscription of await user.related("pushSubscriptions").query()) {
      try {
        const payload = await notification.toWebPush(user);
        await this.client.sendNotification(
          JSON.parse(pushSubscription.payload),
          typeof payload == "object" ? JSON.stringify(payload) : payload
        );
      } catch (e) {
        if (e.statusCode == 410) {
          // Push disabled, remove subscription
          await pushSubscription.delete();
        }
      }
    }
  }
}
