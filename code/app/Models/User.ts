import { DateTime } from "luxon";
import {
  BaseModel,
  beforeSave,
  column,
  HasMany,
  hasMany,
  HasManyThrough,
  hasManyThrough,
} from "@ioc:Adonis/Lucid/Orm";
import Hash from "@ioc:Adonis/Core/Hash";
import { Field, ObjectType } from "type-graphql";
import Workspace from "./Workspace";
import Task from "./Task";
import Tag from "./Tag";
import TagTask from "./TagTask";
import PushSubscription from "./PushSubscription";

@ObjectType()
export default class User extends BaseModel {
  @Field()
  @column({ isPrimary: true })
  public id: number;

  @Field()
  @column()
  public name: string;

  @Field()
  @column()
  public email: string;

  @column({ serializeAs: null })
  public password: string;

  @hasMany(() => PushSubscription)
  public pushSubscriptions: HasMany<typeof PushSubscription>;

  @Field(() => [Workspace])
  @hasMany(() => Workspace)
  public workspaces: HasMany<typeof Workspace>;

  @Field(() => String)
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @Field(() => String)
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @Field(() => [Task])
  @hasMany(() => Task)
  public tasks: HasMany<typeof Task>;

  @Field(() => [Tag])
  @hasMany(() => Tag)
  public tags: HasMany<typeof Tag>;

  @Field(() => [TagTask])
  @hasManyThrough([() => TagTask, () => Tag])
  public tagtasks: HasManyThrough<typeof TagTask, typeof Tag>;

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password);
    }
  }
}
