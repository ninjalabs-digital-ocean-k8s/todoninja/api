import { DateTime } from "luxon";
import {
  afterCreate,
  afterDelete,
  afterUpdate,
  BaseModel,
  column,
  ManyToMany,
  manyToMany,
} from "@ioc:Adonis/Lucid/Orm";
import { Field, ObjectType } from "type-graphql";
import Task from "./Task";
import pubSub from "@ioc:Apollo/PubSub";

@ObjectType()
export default class Tag extends BaseModel {
  @Field()
  @column({ isPrimary: true })
  public id: number;

  @Field()
  @column()
  public name: string;

  @Field({ nullable: true })
  @column()
  public color?: string;

  @Field()
  @column()
  public userId: number;

  @Field(() => [Task])
  @manyToMany(() => Task)
  public tasks: ManyToMany<typeof Task>;

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @afterCreate()
  public static async publishCreate(tag: Tag) {
    pubSub.publish("tag/create", tag);
  }

  @afterUpdate()
  public static async publishUpdate(tag: Tag) {
    pubSub.publish("tag/update", tag);
  }

  @afterDelete()
  public static async publishDelete(tag: Tag) {
    pubSub.publish("tag/delete", tag);
  }
}
