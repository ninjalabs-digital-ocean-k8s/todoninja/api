import { DateTime } from "luxon";
import {
  BaseModel,
  column,
  belongsTo,
  BelongsTo,
  afterCreate,
  afterUpdate,
  afterDelete,
  manyToMany,
  ManyToMany,
} from "@ioc:Adonis/Lucid/Orm";
import { Field, Int, ObjectType } from "type-graphql";
import pubSub from "@ioc:Apollo/PubSub";
import Workspace from "./Workspace";
import Tag from "./Tag";
import User from "./User";

@ObjectType()
export default class Task extends BaseModel {
  @Field(() => Int)
  @column({ isPrimary: true })
  public id: number;

  @Field()
  @column()
  public title: string;

  @Field({ nullable: true })
  @column()
  public description: string;

  @Field(() => Int, { nullable: true })
  @column()
  public workspaceId?: number;

  @Field(() => Int)
  @column()
  public userId: number;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime()
  public doneAt?: DateTime;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime()
  public todayAt?: DateTime;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime()
  public deadlineAt?: DateTime;

  @column()
  @Field({ nullable: true })
  public waitingFor?: string;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime()
  public postponedUntil?: DateTime;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime()
  public remindAt?: DateTime;

  @column.dateTime()
  public remindedAt?: DateTime;

  @Field(() => DateTime, { nullable: true })
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => Workspace)
  public workspace: BelongsTo<typeof Workspace>;

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>;

  @manyToMany(() => Tag)
  public tags: ManyToMany<typeof Tag>;

  @afterCreate()
  public static async publishCreate(task: Task) {
    pubSub.publish("task/create", task);
  }

  @afterUpdate()
  public static async publishUpdate(task: Task) {
    pubSub.publish("task/update", task);
  }

  @afterDelete()
  public static async publishDelete(task: Task) {
    pubSub.publish("task/delete", task);
  }
}
