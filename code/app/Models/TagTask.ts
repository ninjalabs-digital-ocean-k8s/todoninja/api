import { DateTime } from "luxon";
import {
  afterCreate,
  afterDelete,
  afterUpdate,
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
} from "@ioc:Adonis/Lucid/Orm";
import { Field, ObjectType } from "type-graphql";
import Task from "./Task";
import pubSub from "@ioc:Apollo/PubSub";
import Tag from "./Tag";

@ObjectType()
export default class TagTask extends BaseModel {
  public static table = "tag_task";

  @Field()
  @column({ isPrimary: true })
  public id: number;

  @Field()
  @column()
  public tagId: number;

  @Field()
  @column()
  public taskId?: number;

  @Field(() => [Task])
  @belongsTo(() => Task)
  public task: BelongsTo<typeof Task>;

  @Field(() => [Tag])
  @belongsTo(() => Tag)
  public tag: BelongsTo<typeof Tag>;

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @afterCreate()
  public static async publishCreate(tagtask: TagTask) {
    await tagtask.load("tag");
    pubSub.publish("tagtask/create", tagtask);
  }

  @afterUpdate()
  public static async publishUpdate(tagtask: TagTask) {
    await tagtask.load("tag");
    pubSub.publish("tagtask/update", tagtask);
  }

  @afterDelete()
  public static async publishDelete(tagtask: TagTask) {
    await tagtask.load("tag");
    pubSub.publish("tagtask/delete", tagtask);
  }
}
