import { DateTime } from 'luxon'
import { afterCreate, afterDelete, afterUpdate, BaseModel, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import { Field, ObjectType } from 'type-graphql'
import Task from './Task'
import pubSub from '@ioc:Apollo/PubSub'

@ObjectType()
export default class Workspace extends BaseModel {
  @Field()
  @column({ isPrimary: true })
  public id: number

  @Field()
  @column()
  public name: string

  @Field({ nullable: true })
  @column()
  public color?: string

  @Field({ nullable: true })
  @column()
  public icon?: string

  @Field()
  @column()
  public userId: number

  @Field(() => [Task])
  @hasMany(() => Task)
  public tasks: HasMany<typeof Task>

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @Field(() => DateTime)
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime


  @afterCreate()
  public static async publishCreate(workspace: Workspace) {
    pubSub.publish('workspace/create', workspace)
  }

  @afterUpdate()
  public static async publishUpdate(workspace: Workspace) {
    pubSub.publish('workspace/update', workspace)
  }

  @afterDelete()
  public static async publishDelete(workspace: Workspace) {
    pubSub.publish('workspace/delete', workspace)
  }
}
