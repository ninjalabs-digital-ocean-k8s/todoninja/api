import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import { Field, ObjectType } from 'type-graphql'

@ObjectType()
export default class PushSubscription extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @Field()
  @column()
  public payload: string

  @column()
  public userId: number

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
