import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { Arg, Ctx, Field, Mutation, ObjectType, Resolver } from "type-graphql";

@ObjectType()
export class AuthResponse {
  @Field()
  token: string;
}

@Resolver()
export default class AuthResolver {
  @Mutation(() => AuthResponse)
  async generateToken(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() { auth }: HttpContextContract
  ) {
    return {
      token: (await auth.use("api").attempt(email, password)).token,
    };
  }
}
