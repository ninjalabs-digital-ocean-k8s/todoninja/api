import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { Arg, Authorized, Ctx, Field, InputType, Mutation, Resolver } from "type-graphql";
import PushSubscription from "App/Models/PushSubscription";

@InputType()
export class PushSubscriptionCreateInput {
  @Field()
  public payload: string;
}

@Resolver(() => PushSubscription)
export default class PushSubscriptionResolver {
  @Authorized()
  @Mutation(() => PushSubscription)
  public async createPushSubscription(
    @Ctx() { auth }: HttpContextContract,
    @Arg("pushSubscription") input: PushSubscriptionCreateInput
  ) {
    return auth.user?.related("pushSubscriptions").create(input);
  }
}
