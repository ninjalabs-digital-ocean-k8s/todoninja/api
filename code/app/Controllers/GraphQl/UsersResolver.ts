import User from "App/Models/User";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { Arg, Authorized, Ctx, Field, InputType, Mutation, Resolver, Query } from "type-graphql";
import { validator, schema, rules } from "@ioc:Adonis/Core/Validator";

@InputType()
export class UserCreateInput implements Partial<User> {
  @Field()
  name: string;

  @Field()
  email: string;

  @Field()
  password: string;
}

@Resolver(() => User)
export default class UsersResolver {
  @Mutation(() => User)
  async createUser(
    @Arg("user", () => UserCreateInput) userinput: { email: string; password: string }
  ) {
    const data = await validator.validate({
      schema: schema.create({
        name: schema.string({}),
        email: schema.string({}, [rules.email()]),
        password: schema.string({}, [rules.minLength(6)]),
      }),
      data: userinput,
    });
    return await User.create(data);
  }

  @Authorized()
  @Query(() => User)
  public user(@Ctx() { auth }: HttpContextContract) {
    return auth.user!;
  }
}
