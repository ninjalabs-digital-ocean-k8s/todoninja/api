import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Workspace from "App/Models/Workspace";
import {
  Arg,
  Authorized,
  Ctx,
  Field,
  InputType,
  Int,
  Mutation,
  ObjectType,
  Query,
  Resolver,
  Root,
  Subscription,
} from "type-graphql";
import { HasMany } from "@ioc:Adonis/Lucid/Orm";
import { NormalizedQueryType } from "Packages/opaque-type-graphql";
import { parseOpaqueQuery, translateOpaqueQueryToKnexModifier } from "@opaquejs/knex-translator";

@InputType()
export class WorkspacePatchInput {
  @Field({ nullable: true })
  public name?: string;
  @Field({ nullable: true })
  public color?: string;
  @Field({ nullable: true })
  public icon?: string;
}

@InputType()
export class WorkspaceCreateInput extends WorkspacePatchInput {
  @Field()
  public name: string;
}

@ObjectType()
export class DeleteObjectType {
  @Field(() => Int)
  public affectedRows: number;
}

@Resolver(() => Workspace)
export default class WorkspaceResolver {
  @Authorized()
  @Query(() => [Workspace])
  public async workspaces(
    @Ctx() { auth }: HttpContextContract,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});

    return await applyGlobals(auth.user!.related("workspaces").query().where(build));
  }

  @Authorized()
  @Mutation(() => Workspace)
  public createWorkspace(
    @Ctx() { auth }: HttpContextContract,
    @Arg("workspace") workspace: WorkspaceCreateInput
  ) {
    return auth.user!.related("workspaces").create(workspace);
  }

  @Authorized()
  @Mutation(() => [Workspace])
  public async patchWorkspace(
    @Ctx() { auth }: HttpContextContract,
    @Arg("workspace") workspaceInput: WorkspacePatchInput,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});

    const workspaces = await applyGlobals(auth.user!.related("workspaces").query().where(build));

    return await Promise.all(workspaces.map((workspace) => workspace.merge(workspaceInput).save()));
  }

  @Authorized()
  @Mutation(() => DeleteObjectType)
  public async deleteWorkspace(
    @Ctx() { auth }: HttpContextContract,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});

    const workspaces = await applyGlobals(auth.user!.related("workspaces").query().where(build));
    await Promise.all(workspaces.map((workspace) => workspace.delete()));

    return {
      affectedRows: workspaces.length,
    };
  }

  @Authorized()
  @Subscription(() => Workspace, {
    topics: ["workspace/create"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Workspace }) =>
      payload.userId == context.auth.user!.id,
  })
  workspaceCreated(@Root() workspace: Workspace) {
    return workspace;
  }

  @Authorized()
  @Subscription(() => Workspace, {
    topics: ["workspace/update"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Workspace }) =>
      payload.userId == context.auth.user!.id,
  })
  workspaceUpdated(@Root() workspace: Workspace) {
    return workspace;
  }

  @Authorized()
  @Subscription(() => Workspace, {
    topics: ["workspace/delete"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Workspace }) =>
      payload.userId == context.auth.user!.id,
  })
  workspaceDeleted(@Root() workspace: Workspace) {
    return workspace;
  }
}
