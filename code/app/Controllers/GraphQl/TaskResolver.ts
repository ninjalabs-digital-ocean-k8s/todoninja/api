import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Task from "App/Models/Task";
import { DateTime } from "luxon";
import {
  Arg,
  Authorized,
  Ctx,
  Field,
  InputType,
  Mutation,
  Query,
  Resolver,
  Root,
  Subscription,
} from "type-graphql";
import { DeleteObjectType } from "./WorkspaceResolver";
import { parseOpaqueQuery } from "@opaquejs/knex-translator";
import { NormalizedQueryType } from "Packages/opaque-type-graphql";

@InputType()
export class TaskPatchInput {
  @Field({ nullable: true })
  public title?: string;
  @Field({ nullable: true })
  public description?: string;
  @Field({ nullable: true })
  public workspaceId?: number;
  @Field({ nullable: true })
  public doneAt?: DateTime;
  @Field({ nullable: true })
  public todayAt?: DateTime;
  @Field({ nullable: true })
  public postponedUntil?: DateTime;
  @Field({ nullable: true })
  public deadlineAt?: DateTime;
  @Field({ nullable: true })
  public waitingFor?: string;
  @Field({ nullable: true })
  public remindAt?: DateTime;
}

@InputType()
export class TaskCreateInput extends TaskPatchInput {
  @Field({ nullable: false })
  public title: string;
}

@Resolver(() => Task)
export default class TaskResolver {
  @Authorized()
  @Query(() => [Task])
  public async tasks(
    @Ctx() { auth }: HttpContextContract,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});
    return await applyGlobals(auth.user!.related("tasks").query().where(build));
  }

  @Authorized()
  @Mutation(() => Task)
  public async createTask(
    @Ctx() { auth }: HttpContextContract,
    @Arg("task") taskinput: TaskCreateInput
  ) {
    const workspace = taskinput.workspaceId
      ? await auth.user!.related("workspaces").query().where("id", taskinput.workspaceId).first()
      : undefined;

    const task = await auth.user!.related("tasks").create({
      ...taskinput,
      workspaceId: workspace?.id,
    });

    return task;
  }

  @Authorized()
  @Mutation(() => [Task])
  public async patchTask(
    @Ctx() { auth }: HttpContextContract,
    @Arg("task") taskinput: TaskPatchInput,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});

    const tasks = await applyGlobals(auth.user!.related("tasks").query().where(build));

    return await Promise.all(tasks.map((task) => task.merge(taskinput).save()));
  }

  @Authorized()
  @Mutation(() => DeleteObjectType)
  public async deleteTask(
    @Ctx() { auth }: HttpContextContract,
    @Arg("query", { nullable: true }) query?: NormalizedQueryType
  ) {
    const { build, applyGlobals } = parseOpaqueQuery(query || {});

    const tasks = await applyGlobals(auth.user!.related("tasks").query().where(build));
    await Promise.all(tasks.map((task) => task.delete()));

    return {
      affectedRows: tasks.length,
    };
  }

  @Authorized()
  @Subscription(() => Task, {
    topics: ["task/create"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Task }) =>
      payload.userId == context.auth.user!.id,
  })
  taskCreated(@Root() task: Task) {
    return task;
  }

  @Authorized()
  @Subscription(() => Task, {
    topics: ["task/update"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Task }) =>
      payload.userId == context.auth.user!.id,
  })
  taskUpdated(@Root() task: Task) {
    return task;
  }

  @Authorized()
  @Subscription(() => Task, {
    topics: ["task/delete"],
    filter: ({ context, payload }: { context: HttpContextContract; payload: Task }) =>
      payload.userId == context.auth.user!.id,
  })
  taskDeleted(@Root() task: Task) {
    return task;
  }
}
