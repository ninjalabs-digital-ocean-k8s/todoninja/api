import Task from "App/Models/Task";
import TaskReminder from "App/Notifications/TaskReminder";
import { DateTime } from "luxon";

export default class SendTaskReminders {
  constructor(public now: DateTime) {}

  query() {
    return Task.query()
      .where("remind_at", "<=", this.now.toSQL())
      .andWhere((query) =>
        query.whereColumn("reminded_at", "<", "remind_at").orWhereNull("reminded_at")
      );
  }

  async handle() {
    const tasks = await this.query().preload("user");

    tasks.forEach(async (task) => {
      await new TaskReminder(task).send(task.user);
      task.remindedAt = DateTime.now();
      await task.save();
    });
  }
}
